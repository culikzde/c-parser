
/* INP.H */

#ifndef INP_H
#define INP_H

#include <string>

#define NUL        0
#define TAB        9
#define CR        13
#define LF        10
#define CTRLZ     26

/********************************* INPUT **********************************/

extern char ch;         /* current character */

extern int  InpPos;     /* input position */
extern int  InpColNum;  /* column number */
extern int  InpLineNum; /* line number */

void OpenInp (void);    /* open input file */
void CloseInp (void);   /* close input file */
void NextChar (void);   /* get next character */

/***************************** ERROR MESSAGES *****************************/

void Error (char * msg); /* show error message */

#endif
