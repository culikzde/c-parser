
#include "win.h"

#include <QApplication>
#include <QFileDialog>
#include <QFontDialog>
#include <QTextStream>
#include <QMessageBox>

#include "inp.h"
#include "out.h"
#include "lex.h"
#include "decl.h"

#include "ifc.h"

#include <string>
using std::string;

#include <sstream>

MainWindow * win;

void run ();
void setStatus (string msg);

/******************************* MAIN WINDOW ******************************/

MainWindow::MainWindow (QWidget *parent) :
    QMainWindow (parent),
    Ui::MainWindow ()
{
    setupUi (this);
}

MainWindow::~MainWindow()
{
}

void MainWindow::readFile (QString fileName)
{
    QFile file (fileName);
    QString text = "";
    if (file.open (QFile::ReadOnly | QFile::Text))
    {
       QTextStream stream (&file);
       text = stream.readAll ();
       file.close ();
    }
    input->setText (text);
}

void MainWindow::on_actionOpen_triggered ()
{
    QString fileName = QFileDialog::getOpenFileName (this);
    if (fileName != "")
        readFile (fileName);
}

void MainWindow::on_actionSave_triggered()
{
    QString fileName = QFileDialog::getSaveFileName (this);
    if (fileName != "")
    {
       QFile file (fileName);
       if (file.open (QFile::ReadWrite))
       {
          QTextStream stream (&file);
          QString text = input->toPlainText ();
          stream << text;
          file.close ();
       }
    }

}

void MainWindow::on_actionFont_triggered()
{
    bool ok;
    QFont font = QFontDialog::getFont (&ok, input->font (), this);
    if (ok)
    {
        input->setFont (font);
        output->setFont (font);
    }
}

void MainWindow::on_actionClearOutput_triggered()
{
    clearOutLines ();
}

void MainWindow::on_actionRun_triggered()
{
    run ();
}

void MainWindow::on_actionQuit_triggered ()
{
    close ();
}

/********************************** IFC ***********************************/

string getInpText ()
{
    return win->input->toPlainText ().toStdString();
}

void clearOutLines ()
{
    win->output->clear ();
}

void showOutLine (string text)
{
    win->output->append (text.c_str ());
}

/******************************* SHOW ERROR *******************************/

class CompilerException : public std::exception
{
   private:
      string msg;

   public:
      CompilerException (const string p_msg) throw () :
         msg (p_msg)
      {
         /* nothing */
      }

      virtual ~ CompilerException () throw ()
      {
      }

      virtual const char * what () const throw ()
      {
         string txt = "CompilerException: " + msg;
         return txt.c_str ();
      }
};

void setStatus (string txt)
{
    win->QMainWindow::statusBar()->showMessage (QString::fromStdString (txt));
}

string IntToStr (int n)
{
   std::ostringstream stream;
   stream << n;
   return stream.str ();
}

void showError (string txt)
{
    CloseInp (); /* uzavri vstup a vystup */
    CloseOut ();

    QColor color = win->output->textColor ();
    win->output->setTextColor (Qt::red);
    win->output->append (txt.c_str ());
    win->output->setTextColor (color);

    setStatus ("Line: " + IntToStr (InpLineNum) + " " +
               "Col: " + IntToStr (InpColNum) + " " +
               "Error: " + txt);

    QTextCursor cursor = win->input->textCursor ();
    cursor.setPosition (InpPos); /* pozice kurzoru ve vstupnim okenku */

    QMessageBox::critical (win, "Error", QString::fromStdString (txt));

    throw CompilerException (txt); /* skok zpet do funkce run */
}

/********************************** RUN ***********************************/

void run ()
{
    try
    {
        OpenInp ();  /* otevri vstupni a vystupni text */
        OpenOut ();
        InitLex ();  /* inicializace modulu Lex, precteni prvniho symbolu */

        InitDecl (); /* inicializace modulu Decl */

        while (sy!=EOS)
        {
            #if 0
            ExprPtr e = expression (); /* precti vyraz */
            PutExpr (e); /* vytiskni vyraz */
            PutEol ();
            CheckSymbol (SEMICOLON);
            #endif

            #if 0
            StatPtr s = statement ();
            PutStat (s);
            #endif

            #if 1
            GlobalDecl (); /* precti deklarace */
            #endif
        }

        PutGlobalDecl (); /* vytiskni deklarace */

        CloseInp (); /* uzavri vstupni a vystupni text */
        CloseOut ();
        setStatus ("O.K."); /* stavova radka */
    }
    catch (CompilerException & e)
    {
        /* nic - informace o chybe jiz zobrazila funkce ShowError */
    }
}

int main (int argc, char *argv[])
{
    QApplication a (argc, argv);
    win = new MainWindow ();
    // win->input->setFont (QFont ("Sans Serif", 24));
    // win->output->setFont (win->input->font ());
    win->readFile ("../comp/data/test.inp");
    win->show ();
    return a.exec();
}



