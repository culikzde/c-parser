#ifndef MAIN_H
#define MAIN_H

#include "ui_win.h"
#include <QMainWindow>

namespace Ui
{
   class MainWidow;
}

class MainWindow : public QMainWindow, public Ui::MainWindow
{
Q_OBJECT

public:
    explicit MainWindow (QWidget *parent = 0);
    ~MainWindow();
    void readFile (QString fileName);

private slots:
    void on_actionFont_triggered ();
    void on_actionQuit_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionClearOutput_triggered();
    void on_actionRun_triggered();
};

#endif // MAIN_H
