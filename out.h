
/* OUT.H */

#ifndef OUT_H
#define OUT_H

void OpenOut   (void);
void CloseOut  (void);

void Indent (void);
void Unindent (void);

void PutChr (char c);
void PutStr (char * txt);
void PutEol (void);

#endif
