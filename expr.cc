
/* EXPR.C */

#include <string.h>
#include <stdlib.h> /* calloc */

#include "decl.h"

/******************************* UTILITIES ********************************/

void ErrorId (char * msg, char * id)
{
  char txt [256];
  strmov (txt, sizeof (txt), msg);
  stradd (txt, sizeof (txt), " ");
  stradd (txt, sizeof (txt), id);
  Error (txt);
}

void Bug (void)
{
  Error ("Internal error");
}

void Check (bool b)
{
  if (!b) Bug ();
}

void * New (unsigned size)
/* allocate and clear memory */
{
  return calloc (1, size);
}

char * NewStr (char * s)
/* allocate memory for string */
{
  char * p;
  p = (char *) New (strlen(s)+1);
  strcpy (p, s);
  return p;
}

IDENT_PTR ReadIdentPtr (void)
/* precti ze vstupu libovolny identifikator */
{
  if (sy!=IDENT) Error ("Identifier expected");
  char * id = NewStr (IdentVal);
  NextSymbol ();
  return id;
}

void ReadIdent (IDENT_TYP id)
/* precti ze vstupu libovolny identifikator */
{
  if (sy!=IDENT) Error ("Identifier expected");
  strmov (id, sizeof (IDENT_TYP), IdentVal);
  NextSymbol ();
}

/****************************** TYPE LEVELS *******************************/

#define IntLev          0
#define UnsignedLev     1
#define LongLev         2
#define UnsignedLongLev 3
#define FloatLev        4
#define DoubleLev       5
#define LongDoubleLev   6
#define OtherLev        7

Type IntDef          = { Int, Signed },
     UnsignedDef     = { Int, Unsigned },
     LongDef         = { Int, Signed, Long },
     UnsignedLongDef = { Int, Unsigned, Long },
     FloatDef        = { Float },
     DoubleDef       = { Double },
     LongDoubleDef   = { Double, NoSign, Long },

     VoidDef         = { Void },
     BoolDef         = { Int },
     CharDef         = { Char },

     StringDef       = { Pointer, NoSign, NoLength, false, false, &CharDef };

TypePtr Std [7] = { &IntDef,
                    &UnsignedDef,
                    &LongDef,
                    &UnsignedLongDef,
                    &FloatDef,
                    &DoubleDef,
                    &LongDoubleDef };

int CalcLevel (TypePtr typ)
/* Urci kategorii, do ktere patri zadany typ */
{
  switch (typ->sort)
  {
     case Char:
     case Enum:
        return IntLev;

     case NoSort:
     case Int:
        if (typ->length!=Long)
           if (typ->sign!=Unsigned)
              return IntLev;
           else
              return UnsignedLev;
        else
           if (typ->sign!=Unsigned)
              return LongLev;
           else
              return UnsignedLongLev;

     case Float:
        return FloatLev;

     case Double:
        if (typ->length!=Long)
           return DoubleLev;
        else
           return LongDoubleLev;

     default:
        return OtherLev;
  }
}

/***************************** TYPE CHECKING ******************************/

bool IsInteger (TypePtr t)
/* test, zda t je celociselny typ - Char, Enum, Int */
{
  return IsIntegerSort (t->sort);
}

bool IsFloat (TypePtr t)
/* test, zda t je realny typ */
{
  return IsFloatSort (t->sort);
}

bool IsNumeric (TypePtr t)
/* test, zda t je celociselny nebo realny typ */
{
  return IsNumericSort (t->sort);
}

bool IsPointer (TypePtr t)
/* test, zda t je ukazatel - Pointer */
{
  return t->sort==Pointer;
}

bool IsPtr (TypePtr t)
/* test, zda t je ukazatel, odkaz nebo pole - Pointer, Reference, Array */
{
  return IsPtrSort (t->sort);
}

bool IsOperand (TypePtr t)
/* test, zda t je celociselny typ, realny typ nebo ukazatel */
/* Char, Enum, Int, Float, Double, Pointer */
{
  while (t->sort == Reference)
     t = t->from;
  return IsOperandSort (t->sort);
}

bool IsString (TypePtr t)
/* test, zda t je retezec */
{
  sort_typ sort;
  TypePtr from;

  sort = t->sort;
  from = t->from;
  return (sort==Pointer || sort==Array) && from->sort==Char;
}

bool IsText (TypePtr t)
{
  sort_typ sort;
  TypePtr from;

  sort = t->sort;
  from = t->from;
  return sort==Array && from->sort==Char;
}

/* ---------------------------------------------------------------------- */

void InvOp (void)
{
  Error ("Invalid operand type");
}

void CheckInteger (TypePtr t)
/* kontrola, zda t je celociselny typ - Char, Enum, Int */
{
  if (!IsInteger (t)) InvOp ();
}

void inline CheckNumeric (TypePtr t)
/* kontrola, zda t je celociselny nebo realny typ */
/* Char, Enum, Int, Float, Double */
{
  if (!IsNumeric (t)) InvOp ();
}

void CheckOperand (TypePtr t)
/* kontrola, zda t je celociselny typ, realny typ nebo ukazatel */
/* Char, Enum, Int, Float, Double, Pointer */
{
  if (!IsOperand (t)) InvOp ();
}

void CheckPtr (TypePtr t)
/* kontrola, zda t je ukazatel, odkaz nebo pole - Pointer, Reference, Array */
{
  if (!IsPtr (t)) InvOp ();
}

void CheckString (TypePtr t)
/* kontrola, zda t je retezec */
{
  if (!IsString (t)) InvOp ();
}

/* ---------------------------------------------------------------------- */

TypePtr integers (TypePtr t1, TypePtr t2)
/* operace s celociselnymi operandy */
{
  int a, b;

  a = CalcLevel (t1);
  b = CalcLevel (t2);
  if (a<b) a=b;

  if (a>UnsignedLongLev) InvOp ();
  return Std[a];
}

TypePtr arit (TypePtr t1, TypePtr t2)
/* operace s numerickymi operandy */
{
  int a, b;

  a = CalcLevel (t1);
  b = CalcLevel (t2);
  if (a<b) a=b;

  if (a==OtherLev) InvOp ();
  return Std[a];
}

TypePtr pointers (TypePtr t1, TypePtr t2)
/* kontrola, zda t1 a t2 jsou ukazatele stejneho typu */
{
  if (IsInteger (t1))
  {
     CheckPtr (t2);
     return (t2);
  }
  if (IsInteger (t2))
  {
     CheckPtr (t1);
     return (t1);
  }

  CheckPtr (t1);
  CheckPtr (t2);
  return (t1);
}

void compare (TypePtr t1, TypePtr t2)
/* kontrola, zda lze porovnavat typy t1 a t2 */
{
  if (IsInteger (t1))
     CheckOperand (t2);
  else if (IsFloat (t1))
     CheckNumeric (t2);
  else if (IsPtr (t1))
     pointers (t1, t2);
  else InvOp ();
}

void CheckLValue (ExprPtr e)
{
  bool ok;
  SYMBOL op;
  TypePtr t;
  sort_typ sort;

  ok = true;

  while (e->op == LPAR)
     e = e->e1;

  op = e->op;
  if (op!=IDENT &&
      op!=LBRACK &&
      op!=PERIOD &&
      op!=ARROW &&
      op!=DEREF)
      ok = false;

  t = e->type;
  if (t->Const) ok = false;

  if (!IsOperand (t)) /* char, number, pointer */
  {
     sort = t->sort;
     if (sort!=Struct && sort!=Union && sort!=Class)
        ok = false;
  }

  if (!ok) Error ("lvalue expected");
}

void CheckAssign (TypePtr t1, TypePtr t2)
/* kontrola, zda promenne typu t1 lze priradit vyraz typu t2 */
{
  if (t1!=t2)
     compare (t1, t2);
}

void CheckCall (TypePtr t, ExprPtr exp)
/* kontrola, zda funkce typu t muze byt volana s parametry exp */
{
  NodePtr par; /* formal parameter */
  ExprPtr arg; /* argument expression */

  if (t->sort != Function) Error ("Call of nonfunction");
  par = t->list.first;

  for (;;)
  {
     if (par!=NULL)
     {
        TypePtr ptyp = par->item->type;
        if (ptyp==NULL)
           return; /* ... => O.K. */
        if (ptyp->sort==Void)
           par = NULL;
     }

     /* get argument expression */
     /* prepare next expression */
     if (exp!=NULL && exp->op==COMMA)
     {
        arg = exp->e1;
        exp = exp->e2;
     }
     else
     {
        arg = exp;
        exp = NULL;
     }

     if (par==NULL || arg==NULL) break;
     CheckAssign (par->item->type, arg->type);

     /* next formal parameter */
     par = par->next;
  }

  if (par!=NULL)
     Error ("Too few parameters in function call");
  if (arg!=NULL)
     Error ("Extra parameter in function call");
}

TypePtr add (TypePtr t1, TypePtr t2)
/* kontrola, zda lze vyrazy typu t1 a t2 scitat */
/* t1 a t2 mouhou byt NULL */
{
  TypePtr res;

  if (t1==NULL) t1 = &IntDef;
  if (t2==NULL) t2 = &IntDef;

  if (IsInteger (t1))
  {
     if (IsPtr (t2))
        res = t2;
     else
        res = arit (t1, t2);
  }
  else if (IsFloat (t1))
  {
     res = arit (t1, t2);
  }
  else if (IsPtr (t1))
  {
     CheckInteger (t2);
     res = t1;
  }
  else InvOp ();

  return (res);
}

TypePtr sub (TypePtr t1, TypePtr t2)
/* kontrola, zda lze vyrazy typu t1 a t2 odcitat */
/* t1 a t2 mouhou byt NULL */
{
  TypePtr res;

  if (t1==NULL) t1 = &IntDef;
  if (t2==NULL) t2 = &IntDef;

  if (IsNumeric (t1))
     res = arit (t1, t2);
  else if (IsPtr (t1))
  {
     res = t1;
     if (IsPtr (t2))
        pointers (t1, t2);
     else
        CheckInteger (t2);
  }
  else InvOp ();

  return (res);
}

/********************************** CALC **********************************/

ExprPtr calc (ExprPtr e1, ExprPtr e2, SYMBOL op)
/* Vytvor vyraz z operandu 'e1', 'e2' a operatoru 'op', urci typ vysledku */
{
  ExprPtr ans;
  TypePtr t1 = NULL;
  TypePtr t2 = NULL;
  TypePtr res = NULL;

  if (e1!=NULL) t1 = e1->type;
  if (e2!=NULL) t2 = e2->type;

  switch (op)
  {
     case LPAR:    /* ( ) */
        res = t1;
        break;

     case LBRACK:  /* [ ] */
        if (t1->sort!=Array && t1->sort!=Pointer)
           Error ("Invalid indirection");
        CheckAssign (&IntDef, t2);

        res = t1->from;
        break;

     case FCE:     /* ( ) */
        CheckCall (t1, e2);
        res = t1->from;
        break;

     case ADR:     /* & */
        res = Link (Pointer, t2);
        break;

     case DEREF:   /* * */
        if (t2->sort != Pointer) Error ("Invalid indirection");
        res = t2->from;
        break;

     case SIZEOF_:  /* sizeof */
        res = &UnsignedDef;
        break;

     case NOT:     /* ~ */
        CheckNumeric (t2);
        res = t2;
        break;

     case LOGNOT:  /* ! */
        CheckOperand (t2);
        res = &IntDef;
        break;

     case DPLUS:   /* ++e2 and e1++ */
     case PLUS:    /* +e2 and e1+e2 */
        res = add (t1, t2);
        break;

     case DMINUS:  /* --e2 and e1-- */
     case MINUS:   /* -e2 and e1-e2 */
        res = sub (t1, t2);
        break;

     case ASTERISK:
     case SLASH:
        res = arit (t1, t2);
        break;

     case MOD:
     case SHL:
     case SHR:
     case AND:
     case XOR:
     case OR:
        res = integers (t1, t2);
        break;

     case LESS:
     case GREATER:
     case LESSEQUAL:
     case GREATEREQUAL:
        compare (t1, t2);
        res = &IntDef;
        break;

     case EQUAL:
     case UNEQUAL:
        compare (t1, t2);
        res = &IntDef;
        break;

     case LOGAND:
     case LOGOR:
        CheckOperand (t1);
        CheckOperand (t2);
        res = &IntDef;
        break;

     case ASSIGN:
        CheckLValue (e1);
        CheckAssign (t1, t2);
        res = t1;
        break;

     case ADDASSIGN:
        CheckLValue (e1);
        add (t1, t2);
        CheckAssign (t1, t2);
        res = t1;
        break;

     case SUBASSIGN:
        sub (t1, t2);
        CheckAssign (t1, t2);
        res = t1;
        break;

     case MULASSIGN:
     case DIVASSIGN:
        CheckLValue (e1);
        arit (t1, t2);
        CheckAssign (t1, t2);
        res = t1;
        break;

     case MODASSIGN:
     case SHLASSIGN:
     case SHRASSIGN:
     case ANDASSIGN:
     case XORASSIGN:
     case ORASSIGN:
        CheckLValue (e1);
        integers (t1, t2);
        CheckAssign (t1, t2);
        res = t1;
        break;

     case COMMA:
        res = t2;
        break;

     case LBRACE:
        res = &VoidDef; /* !!! */
        break;

     default:
        Bug ();
  }

  ans = NEW (Expr);
  ans->op = op;
  ans->e1 = e1;
  ans->e2 = e2;
  ans->type = res;

  return (ans);
}

ExprPtr calc3 (ExprPtr e1, ExprPtr e2, ExprPtr e3, SYMBOL op)
/* Operace s tremi operandy */
{
  ExprPtr ans;
  TypePtr t1, t2, t3, res;

  t1 = e1->type;
  t2 = e2->type;
  t3 = e3->type;

  CheckOperand (t1);

  if (t2==t3)
     res = t2;
  else if (IsPtr (t2) || IsPtr (t3))
     res = pointers (t2, t3);
  else
     res = arit (t2, t2);

  ans = NEW (Expr);
  ans->op = op;
  ans->e1 = e1;
  ans->e2 = e2;
  ans->e3 = e3;
  ans->type = res;

  return ans;
}

ExprPtr calci (ExprPtr e1, IDENT_TYP id, SYMBOL op)
/* Vytvor vyraz z operandu 'e1', 'id' a operatoru 'op', urci typ vysledku */
{
  ExprPtr ans;
  TypePtr res;

  TypePtr   typ;
  ObjectPtr obj;
  sort_typ  sort;

  res = NULL;
  typ = e1->type;
  sort = typ->sort;

  switch (op)
  {
     case PERIOD:  /* . */
        if (! IsClsSort (sort))
           Error ("Structure required on left side of .");

        obj = FindMember (typ, id);
        res = obj->type;
        break;

     case ARROW:   /* -> */
        if (sort != Pointer)
           Error ("Pointer to structure required on left side of ->");

        typ = typ->from;
        sort = typ->sort;

        if (! IsClsSort (sort))
           Error ("Pointer to structure required on left side of ->");

        obj = FindMember (typ, id);
        res = obj->type;
        break;

     default:
        Bug ();
  }

  ans = NEW (Expr);
  ans->op = op;
  ans->e1 = e1;
  ans->id = id;
  ans->type = res;

  return ans;
}

/******************************* EXPRESSION *******************************/

ExprPtr primary (void)
{
  ExprPtr e;
  char * ans;
  TypePtr res;

  switch (sy)
  {
     case IDENT:
        ans = NewStr (IdentVal);
        res = Find (IdentVal)->type;
        break;

     case NUM:
        ans = NewStr (NumVal);
        if (u_suffix)
        {
           if (l_suffix) res = &UnsignedLongDef;
                     else res = &UnsignedDef;
        }
        else
        {
           if (l_suffix) res = &LongDef;
                    else res = &IntDef;
        }
        break;

     case FLT:
        ans = NewStr (NumVal);
        if (f_suffix) res = &FloatDef;
        else if (l_suffix) res = &LongDoubleDef;
        else res = &DoubleDef;
        break;

     case STR:
        ans = NewStr (StrVal);
        res = &StringDef;
        break;

     case CHR:
        char tmp [2];
        tmp [0] = ChrVal;
        tmp [1] = 0;
        ans = NewStr (tmp);
        res = &CharDef;
        break;

     default:
        Error ("Syntax error in expression");
  }

  e = NEW (Expr);
  e->op = sy;
  e->id = ans;
  e->type = res;

  NextSymbol ();
  return (e);
}

ExprPtr primary_expr (void)
{
  ExprPtr t1;

  if (sy==IDENT ||
      sy==NUM ||
      sy==FLT ||
      sy==STR ||
      sy==CHR)
  {
     t1 = primary ();
  }
  else if (sy==LPAR)
  {
     NextSymbol ();
     t1 = expression ();
     t1 = calc (t1, NULL, LPAR);
     CheckSymbol (RPAR);
  }
  else Error ("Syntax error in expression");

  return (t1);
}

ExprPtr postfix_expr (void)
{
  ExprPtr t1, t2;

  t1 = primary_expr ();

  for (;;)
     if (sy==LBRACK)
     {
        NextSymbol ();
        t2 = expression ();
        t1 = calc (t1, t2, LBRACK);
        CheckSymbol (RBRACK);
     }

     else if (sy==LPAR)
     {
        NextSymbol ();
        /* argument-expression-list */
        if (sy==RPAR)
           t2 = NULL;
        else
           t2 = expression ();
        CheckSymbol (RPAR);
        t1 = calc (t1, t2, FCE);
     }

     else if (sy==PERIOD)
     {
        IDENT_PTR id;
        NextSymbol ();
        id = ReadIdentPtr ();
        t1 = calci (t1, id, PERIOD);
     }

     else if (sy==ARROW)
     {
        IDENT_PTR id;
        NextSymbol ();
        id = ReadIdentPtr ();
        t1 = calci (t1, id, ARROW);
     }

     else if (sy==DPLUS || sy==DMINUS)
     {
        t1 = calc (t1, NULL, sy);
        NextSymbol ();
     }
     else break;

  return (t1);
 }

ExprPtr cast_expr (void);

ExprPtr unary_expr (void)
{
  ExprPtr t1;
  SYMBOL op;

  if (sy==DPLUS || sy==DMINUS)
  {
     op = sy;
     NextSymbol ();
     t1 = unary_expr ();
     t1 = calc (NULL, t1, op);
  }

  else if (sy==PLUS ||
           sy==MINUS ||
           sy==NOT)
  {
     op = sy;
     NextSymbol ();
     t1 = cast_expr ();
     t1 = calc (NULL, t1, op);
  }

  else if (sy==LOGNOT)
  {
     op = sy;
     NextSymbol ();
     t1 = cast_expr ();
     t1 = calc (NULL, t1, op);
  }

  else if (sy==AND)
  {
     NextSymbol ();
     t1 = cast_expr ();
     t1 = calc (NULL, t1, ADR);
  }

  else if (sy==ASTERISK)
  {
     NextSymbol ();
     t1 = cast_expr ();
     if (t1->type->sort!=Pointer) Error ("Invalid indirection");
     t1 = calc (NULL, t1, DEREF);
  }

  else if (sy==SIZEOF_)
  {
     NextSymbol ();
     t1 = unary_expr ();
     t1 = calc (NULL, t1, SIZEOF_);
     /*
     if (sy==LPAR)
     {
        NextSymbol ();
        !!!
        CheckSymbol (RPAR);
     }
     */
  }

  else t1 = postfix_expr ();

  return (t1);
}

ExprPtr cast_expr (void)
{
  /*
  if (sy==LPAR)
  {
     NextSymbol ();
     !!!
     CheckSymbol (RPAR);
  }
  */
  return unary_expr ();
}

ExprPtr mul_expr (void)
{
  ExprPtr t1, t2;
  SYMBOL op;

  t1 = cast_expr ();
  while (sy==ASTERISK ||
         sy==SLASH ||
         sy==MOD)
  {
     op = sy;
     NextSymbol ();
     t2 = cast_expr ();
     t1 = calc (t1, t2, op);
  }
  return (t1);
}

ExprPtr add_expr (void)
{
  ExprPtr t1, t2;
  SYMBOL op;

  t1 = mul_expr ();
  while (sy==PLUS ||
         sy==MINUS)
  {
     op = sy;
     NextSymbol ();
     t2 = mul_expr ();
     t1 = calc (t1, t2, op);
  }

  return (t1);
}

ExprPtr shift_expr (void)
{
  ExprPtr t1, t2;
  SYMBOL op;

  t1 = add_expr ();
  while (sy==SHL ||
         sy==SHR)
  {
     op = sy;
     NextSymbol ();
     t2 = add_expr ();
     t1 = calc (t1, t2, op);
  }

  return (t1);
}

ExprPtr rel_expr (void)
{
  ExprPtr t1, t2;
  SYMBOL op;

  t1 = shift_expr ();
  while (sy==LESS ||
         sy==GREATER ||
         sy==LESSEQUAL ||
         sy==GREATEREQUAL)
  {
     op = sy;
     NextSymbol ();
     t2 = shift_expr ();
     t1 = calc (t1, t2, op);
  }

  return (t1);
}

ExprPtr equ_expr (void)
{
  ExprPtr t1, t2;
  SYMBOL op;

  t1 = rel_expr ();
  while (sy==EQUAL ||
         sy==UNEQUAL)
  {
     op = sy;
     NextSymbol ();
     t2 = rel_expr ();
     t1 = calc (t1, t2, op);
  }

  return (t1);
}

ExprPtr and_expr (void)
{
  ExprPtr t1, t2;

  t1 = equ_expr ();
  while (sy==AND)
  {
     NextSymbol ();
     t2 = equ_expr ();
     t1 = calc (t1, t2, AND);
  }

  return (t1);
}

ExprPtr xor_expr (void)
{
  ExprPtr t1, t2;

  t1 = and_expr ();
  while (sy==XOR)
  {
     NextSymbol ();
     t2 = and_expr ();
     t1 = calc (t1, t2, XOR);
  }

  return (t1);
}

ExprPtr or_expr (void)
{
  ExprPtr t1, t2;

  t1 = xor_expr ();
  while (sy==OR)
  {
     NextSymbol ();
     t2 = xor_expr ();
     t1 = calc (t1, t2, OR);
  }

  return (t1);
}

ExprPtr land_expr (void)
{
  ExprPtr t1, t2;

  t1 = or_expr ();
  while (sy==LOGAND)
  {
     NextSymbol ();
     t2 = or_expr ();
     t1 = calc (t1, t2, LOGAND);
  }

  return (t1);
}

ExprPtr lor_expr (void)
{
  ExprPtr t1, t2;

  t1 = land_expr ();
  while (sy==LOGOR)
  {
     NextSymbol ();
     t2 = land_expr ();
     t1 = calc (t1, t2, LOGOR);
  }

  return (t1);
}

ExprPtr cond_expr (void)
{
  ExprPtr t1, t2, t3;

  t1 = lor_expr ();
  if (sy==QUESTION)
  {
     t2 = expression ();
     CheckSymbol (COLON);
     t3 = cond_expr ();
     t1 = calc3 (t1, t2, t3, QUESTION);
  }

  return (t1);
}

ExprPtr const_expr (void)
{
  /* !!! */
  return cond_expr ();
}

ExprPtr simple_expr (void)
{
  return cond_expr ();
}

ExprPtr assignment_expr (void)
{
  ExprPtr t1, t2;
  SYMBOL op;

  t1 = cond_expr ();
  while (sy==ASSIGN ||
         sy==MULASSIGN ||
         sy==DIVASSIGN ||
         sy==MODASSIGN ||
         sy==ADDASSIGN ||
         sy==SUBASSIGN ||
         sy==SHLASSIGN ||
         sy==SHRASSIGN ||
         sy==ANDASSIGN ||
         sy==XORASSIGN ||
         sy==ORASSIGN)
  {
     op = sy;
     NextSymbol ();
     t2 = cond_expr ();
     t1 = calc (t1, t2, op);
  }

  return (t1);
}

ExprPtr expression (void)
{
  ExprPtr t1, t2;

  t1 = assignment_expr ();
  if (sy==COMMA)
  {
     NextSymbol ();
     t2 = expression ();
     t1 = calc (t1, t2, COMMA);
  }

  return (t1);
}

ExprPtr initializer (void)
{
  ExprPtr t1, t2;

  if (sy==LBRACE)
  {
     NextSymbol ();

     /* initializer_list */
     t1 = initializer ();

     while (sy==COMMA)
     {
        NextSymbol ();
        t2 = initializer ();
        t1 = calc (t1, t2, COMMA);
     }

     t1 = calc (t1, NULL, LBRACE);
     CheckSymbol (RBRACE);
  }
  else
  {
     t1 = assignment_expr ();
  }

  return (t1);
}

/******************************** PUT EXPR ********************************/

void PutInit (ExprPtr t)
{
  switch (t->op)
  {
     case LBRACE:
        PutChr ('{');
        PutInit (t->e1);
        PutChr ('}');
        break;

     case COMMA:
        PutInit (t->e1);

        PutChr (',');
        PutChr (' ');

        PutInit (t->e2);
        break;

     default:
        PutExpr (t);
  }
}

void PutString (char * txt, char quote)
{
   PutChr (quote);

   for (;;)
   {
      char c = *(txt++);
      if (c==0) break;

      if (c>=0 && c<32)
         switch (c)
         {
            case '\a': PutChr (BACKSLASH); PutChr ('a'); break;
            case '\b': PutChr (BACKSLASH); PutChr ('b'); break;
            case '\f': PutChr (BACKSLASH); PutChr ('f'); break;
            case '\n': PutChr (BACKSLASH); PutChr ('n'); break;
            case '\r': PutChr (BACKSLASH); PutChr ('r'); break;
            case '\t': PutChr (BACKSLASH); PutChr ('t'); break;
            case '\v': PutChr (BACKSLASH); PutChr ('v'); break;

            default:
               PutChr (BACKSLASH);
               PutChr ((char) ('0' + c/64));
               PutChr ((char) ('0' + ((c/8) & 7)));
               PutChr ((char) ('0' + (c & 7)));
               break;
         }
      else
      {
         if (c==QUOTE1 || c==QUOTE2 || c==BACKSLASH) PutChr (BACKSLASH);
         PutChr (c);
      }
   }

   PutChr (quote);
}

void PutExpr (ExprPtr t)
{
  if (t==NULL) return;

  switch (t->op)
  {
     case IDENT:
     case NUM:
     case FLT:
        PutStr (t->id);
        break;

     case CHR:
        PutString (t->id, QUOTE1);
        break;

     case STR:
        PutString (t->id, QUOTE2);
        break;

     case LPAR:
        PutChr ('(');
        PutExpr (t->e1);
        PutChr (')');
        break;

     case LBRACK:
        PutExpr (t->e1);
        PutChr ('[');
        PutExpr (t->e2);
        PutChr (']');
        break;

     case LBRACE:
        PutInit (t);
        break;

     case FCE:
        PutExpr (t->e1);
        PutStr (" (");
        PutExpr (t->e2);
        PutChr (')');
        break;

     case PERIOD:
        PutExpr (t->e1);
        PutChr ('.');
        PutStr (t->id);
        break;

     case ARROW:
        PutExpr (t->e1);
        PutStr ("->");
        PutStr (t->id);
        break;

     case ADR:
        PutChr ('&');
        PutExpr (t->e2);
        break;

     case DEREF:
        PutChr ('*');
        PutExpr (t->e2);
        break;

     case DPLUS:
     case DMINUS:
        if (t->e1==NULL) PutChr (' ');
        PutExpr (t->e1);
        PutSymbol (t->op);
        PutExpr (t->e2);
        break;

     case QUESTION:
        PutExpr (t->e1);
        PutChr ('?');
        PutExpr (t->e2);
        PutChr (':');
        PutExpr (t->e3);
        break;

     case COMMA:
        PutExpr (t->e1);
        PutStr (", ");
        PutExpr (t->e2);
        break;

     case ASSIGN:
        PutExpr (t->e1);
        PutSymbol (t->op);
        PutExpr (t->e2);
        break;

     case LOGAND:
     case LOGOR:
        PutExpr (t->e1);
        PutChr (' ');
        PutSymbol (t->op);
        PutChr (' ');
        PutExpr (t->e2);
        break;

     case LOGNOT:
        PutSymbol (t->op);
        PutExpr (t->e2);
        break;

     default:
        PutExpr (t->e1);
        PutSymbol (t->op);
        PutExpr (t->e2);
        break;
  }
}
