
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = comp
TEMPLATE = app

SOURCES += inp.cc \
           out.cc \
           lex.cc \
           expr.cc \
           stat.cc \
           decl.cc \
           win.cc

HEADERS += inp.h \
           out.h \
           lex.h \
           decl.h \
           ifc.h \
           win.h

FORMS   += win.ui
