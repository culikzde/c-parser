
/* STAT.C */

#include <string.h>

#include "decl.h"

/******************************* STATEMENTS *******************************/

ExprPtr BoolExpr (void)
{
  ExprPtr e;
  e = expression ();
  CheckInteger (e->type);
  return e;
}

ExprPtr IntExpr (void)
{
  ExprPtr e;
  e = expression ();
  CheckInteger (e->type);
  return e;
}

ExprPtr StringExpr (void)
{
  ExprPtr e;
  e = expression ();
  CheckString (e->type);
  return e;
}

ExprPtr CallExpr (void)
{
  ExprPtr e;
  SYMBOL op;

  e = expression ();
  op = e->op;

  if (op != DPLUS &&
      op != DMINUS &&
      op != FCE &&
      op != ASSIGN &&
      op != MULASSIGN &&
      op != DIVASSIGN &&
      op != MODASSIGN &&
      op != ADDASSIGN &&
      op != SUBASSIGN &&
      op != SHLASSIGN &&
      op != SHRASSIGN &&
      op != ANDASSIGN &&
      op != XORASSIGN &&
      op != ORASSIGN)
      Error ("Strange statement");

  return e;
}

/* ---------------------------------------------------------------------- */

StatPtr statement (void)
{
  StatPtr s = NULL;

  if (sy==GOTO_)                               /* goto */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = GOTO_;
     s->id = ReadIdentPtr ();
     /* !! kontrola navesti */
     CheckSymbol (SEMICOLON);
  }

  else if (sy==CONTINUE_)                      /* continue */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = CONTINUE_;
     CheckSymbol (SEMICOLON);
  }

  else if (sy==BREAK_)                         /* break */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = BREAK_;
     CheckSymbol (SEMICOLON);
  }

  else if (sy==RETURN_)                        /* return */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = RETURN_;

     if (sy!=SEMICOLON)
     {
        ExprPtr e;
        e = expression ();
        CheckAssign (Fce->type->from, e->type);
        s->e1 = e;
     }

     CheckSymbol (SEMICOLON);
  }

  else if (sy==WHILE_)                         /* while */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = WHILE_;

     CheckSymbol (LPAR);
     s->e1 = BoolExpr ();
     CheckSymbol (RPAR);

     s->s1 = statement ();
  }

  else if (sy==DO_)                            /* do */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = DO_;
     s->s1 = statement ();

     CheckSymbol (WHILE_);
     CheckSymbol (LPAR);
     s->e1 = BoolExpr ();
     CheckSymbol (RPAR);
     CheckSymbol (SEMICOLON);
  }

  else if (sy==FOR_)                           /* for */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = FOR_;

     CheckSymbol (LPAR);

     if (sy!=SEMICOLON) s->e1 = expression ();
     CheckSymbol (SEMICOLON);

     if (sy!=SEMICOLON) s->e2 = BoolExpr ();
     CheckSymbol (SEMICOLON);

     if (sy!=RPAR) s->e3 = expression ();
     CheckSymbol (RPAR);

     s->s1 = statement ();
  }

  else if (sy==IF_)                            /* if */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = IF_;

     CheckSymbol (LPAR);
     s->e1 = BoolExpr ();
     CheckSymbol (RPAR);

     s->s1 = statement ();

     if (sy==ELSE_)
     {
        NextSymbol ();
        s->s2 = statement ();
     }
  }

  else if (sy==SWITCH_)                        /* switch */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = SWITCH_;

     CheckSymbol (LPAR);
     s->e1 = IntExpr ();
     CheckSymbol (RPAR);

     s->s1 = statement ();
  }

  else if (sy==LBRACE)                         /* { } */
  {
     StatPtr p, first, last;

     NextSymbol ();
     s = NEW (Stat);
     s->st = LBRACE;
     OpenScope (&s->scope);

     first = NULL;
     last = NULL;
     while (sy!=RBRACE)
     {
        p = statement ();

        /* pripojeni do fronty */
        if (first==NULL)
           first = p;
        else
           last->next = p;

        last = p;
        Check (p->next == NULL);
     }

     s->s1 = first;
     CloseScope ();
     NextSymbol ();
  }

  else if (sy==CASE_)                          /* case */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = CASE_;
     s->e1 = const_expr ();
     CheckSymbol (COLON);
  }

  else if (sy==DEFAULT_)                       /* default */
  {
     NextSymbol ();
     s = NEW (Stat);
     s->st = DEFAULT_;
     CheckSymbol (COLON);
  }

  else if (IsDecl ())                          /* declaration */
  {
     s = NEW (Stat);
     s->st = DECL;
     declaration (&s->decl, Init);
  }

  else                                         /* label, expression */
  {
     s = NEW (Stat);
     s->st = SIM;
     if (sy != SEMICOLON)
     {
        s->e1 = expression ();

        if (sy==COLON)
        {
           /* !! kontrola navesti */
           NextSymbol ();
           s->st = LAB;
           s->s1 = statement ();
        }
     }

     CheckSymbol (SEMICOLON);
  }

  return s;
}

/***************************** PUT STATEMENT ******************************/

void SubStat (StatPtr s)
{
  Check (s!=NULL);

  if (s->st == LBRACE) /* graficka uprava */
     PutStat (s);
  else
  {
     Indent ();
     PutStat (s);
     Unindent ();
  }
}

void PutStat (StatPtr s)
{
  Check (s!=NULL);

  switch (s->st)
  {
     case GOTO_:
        PutStr ("goto ");
        PutStr (s->id);
        PutStr (";");
        PutEol ();
        break;

     case CONTINUE_:
        PutStr ("continue;");
        PutEol ();
        break;

     case BREAK_:
        PutStr ("break;");
        PutEol ();
        break;

     case RETURN_:
        PutStr ("return");
        if (s->e1 != NULL)
        {
           PutChr (' ');
           PutExpr (s->e1);
        }
        PutChr (';');
        PutEol ();
        break;

     case WHILE_:
        PutStr ("while (");
        PutExpr (s->e1);
        PutStr (")");
        PutEol ();
        SubStat (s->s1);
        break;

     case DO_:
        PutStr ("do");
        PutEol ();
        SubStat (s->s1);
        PutStr ("while (");
        PutExpr (s->e1);
        PutStr (");");
        PutEol ();
        break;

     case FOR_:
        PutStr ("for (");
        if (s->e1!=NULL) PutExpr (s->e1);
        PutStr ("; ");
        if (s->e2!=NULL) PutExpr (s->e2);
        PutStr ("; ");
        if (s->e3!=NULL) PutExpr (s->e3);
        PutStr (")");
        PutEol ();
        SubStat (s->s1);
        break;

     case IF_:
        PutStr ("if (");
        PutExpr (s->e1);
        PutStr (")");
        PutEol ();

        SubStat (s->s1);

        if (s->s2 != NULL)
        {
           StatPtr t;
           t = s->s2;

           if (t->st == IF_) /* graficka uprava */
           {
              PutStr ("else ");
              PutStat (t);
           }
           else
           {
              PutStr ("else");
              PutEol ();
              SubStat (t);
           }
        }
        break;

     case SWITCH_:
        PutStr ("switch (");
        PutExpr (s->e1);
        PutStr (")");
        PutEol ();
        SubStat (s->s1);
        break;

     case LBRACE:
        {
           StatPtr p;

           PutChr ('{');
           PutEol ();
           Indent ();

           for (p=s->s1; p!=NULL; p=p->next)
              PutStat (p);

           Unindent ();
           PutChr ('}');
           PutEol ();
        }
        break;

     case CASE_:
        PutStr ("case ");
        PutExpr (s->e1);
        PutChr (':');
        PutEol ();
        break;

     case DEFAULT_:
        PutStr ("default:");
        PutEol ();
        break;

     case LAB:
        PutStr (s->id);
        PutChr (':');
        PutStat (s->s1);
        PutEol ();
        break;

     case SIM:
        if (s->e1 != NULL) PutExpr (s->e1);
        PutChr (';');
        PutEol ();
        break;

     case DECL:
        {
           NodePtr p;
           for (p=s->decl.first; p!=NULL; p=p->next)
              PutDecl (p->item);
        }
        break;
  }
}
