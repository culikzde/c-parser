
/* DECL.C */

#include "decl.h"
#include <string.h>

Scope     gscope;
Queue     glist;

ObjectPtr Fce;

int DisplayLevel;
ScopePtr Display [DISPLAY_MAX];

/********************************** INIT **********************************/

void InitDclSym (void);

void InitDecl (void)
{
  InitDclSym ();

  gscope.root = NULL;
  Clear (&glist);
  Fce = NULL;

  DisplayLevel = 0;
  CLR_ARR (Display);
  Display [0] = &gscope;
}

/********************************* QUEUE **********************************/

void Clear (QueuePtr queue)
{
   queue->first = NULL;
   queue->last = NULL;
}

void Add (QueuePtr queue, NodePtr node)
{
  node->prev = queue->last;
  node->next = NULL;

  if (queue->first==NULL)
     queue->first = node;
  else
     queue->last->next = node;

  queue->last = node;
}

void Append (QueuePtr queue, ObjectPtr obj)
{
  NodePtr decl;

  Check (obj!=NULL);
  decl = NEW (Node);
  decl->item = obj;
  Add (queue, decl);
}

void Join (QueuePtr dst, QueuePtr add)
/* Pridej prvky fronty 'add' do fronty 'dst' */
{
  if (add->first!=NULL)
     if (dst->first==NULL)
         *dst = *add;
     else
     {
         dst->last->next = add->first;
         add->first->prev = dst->last;
         dst->last = add->last;
     }
}

/********************************* SCOPE **********************************/

void OpenScope (ScopePtr s)
{
  DisplayLevel ++;
  if (DisplayLevel >= DISPLAY_MAX) Error ("Too many nested scope levels");

  Display [DisplayLevel] = s;
}

void CloseScope (void)
{
  Check (DisplayLevel > 0);
  DisplayLevel --;
}

/********************************** TREE ***********************************/

void EnterPrim (ObjectPtr * root, ObjectPtr add)
/* Vloz identifikator do stromu */
{
  int cmp;

  for (;;)
     if (*root==NULL)
     {
        *root = add;
        add->left = NULL;
        add->right = NULL;
        return;
     }
     else
     {
        cmp = strcmp (add->name, (*root)->name);
        if (cmp < 0)
           root = &(*root)->left;
        else if (cmp>0)
           root = &(*root)->right;
        else
           ErrorId ("Duplicated identifier", add->name);
     }
}

void Enter (ScopePtr scope, ObjectPtr add)
/* Vloz identifikator do stromu */
{
  EnterPrim (&scope->root, add);
}

/* ---------------------------------------------------------------------- */

ObjectPtr SearchPrim (ObjectPtr root, IDENT_TYP name)
/* Hledej zadany identifikator ve stromu */
{
  int cmp;

  for (;;)
     if (root==NULL)
        return (NULL);
     else
     {
        cmp = strcmp (name, root->name);
        if (cmp<0)
           root = root->left;
        else if (cmp>0)
           root = root->right;
        else
           return (root);
     }
}

ObjectPtr Search (ScopePtr scope, IDENT_TYP name)
/* Hledej zadany identifikator ve stromu */
{
   return SearchPrim (scope->root, name);
}

/* ---------------------------------------------------------------------- */

ObjectPtr Lookup (IDENT_TYP name)
/* Hledej zadany identifikator ve vsech stromech */
{
  int i;
  ObjectPtr obj;

  i = DisplayLevel;
  obj = Search (Display[i], name);
  while (obj==NULL && i>0)
  {
     i--;
     obj = Search (Display[i], name);
  }

  return (obj);
}

ObjectPtr Find (IDENT_TYP name)
/* Hledej zadany identifikator. Ohlas chybu, pokud nebyl nalezen. */
{
  ObjectPtr obj;

  obj = Lookup (name);
  if (obj==NULL) ErrorId ("Unknown identifier", name);
  return (obj);
}

/* ---------------------------------------------------------------------- */

ObjectPtr LookupMember (TypePtr typ, IDENT_TYP name)
/* Hledej polozku struktury */
{
  return Search (&typ->scope, name);
}

ObjectPtr FindMember (TypePtr typ, IDENT_TYP name)
/* Hledej polozku struktury. Ohlas chybu, pokud nebyla nalezena. */
{
  ObjectPtr obj;

  obj = LookupMember (typ, name);
  if (obj==NULL)
     ErrorId ("Member of structure expected", name);
  return (obj);
}

/************************ CREATE TYPES AND OBJECTS ************************/

TypePtr NewType (sort_typ sort)
/* Vytvor novy typ, nastav polozku sort */
{
  TypePtr typ;

  typ = NEW (Type);
  typ->sort = sort;

  return typ;
}

TypePtr Resolve (TypePtr typ)
{
  if (typ==NULL)
     return NULL;

  if (typ->sort==Name || typ->sort==Tag)
     typ = typ->from;

  return typ;
}

TypePtr Link (sort_typ sort, TypePtr desc)
{
  TypePtr typ;

  typ = NewType (sort);
  typ->desc = desc;
  typ->from = Resolve (desc);

  return typ;
}

TypePtr Alias (IDENT_TYP name, sort_typ sort, TypePtr desc)
{
  TypePtr typ;

  Check (sort==Name || sort==Tag);
  typ = Link (sort, desc);
  typ->tag = NewStr (name);

  return typ;
}

ObjectPtr NewObject (IDENT_TYP name, TypePtr desc)
/* Vytvor objekt daneho jmena a typu */
{
  ObjectPtr obj;

  obj = NEW (Object);
  obj->name = NewStr (name);
  obj->desc = desc;
  obj->type = Resolve (desc);

  return (obj);
}

/* ---------------------------------------------------------------------- */

TypePtr CreateType (IDENT_TYP id, sort_typ sort, bool def)
{
  ObjectPtr obj;
  TypePtr   typ;

  if (id==NULL || id[0]==0)
  {
     /* anonymous */
     typ = NewType (sort);
     if (!def) Error ("{ expected");
  }
  else
  {
     obj = Search (Display[DisplayLevel], id);
     if (obj==NULL)
     {
        /* new type */
        typ = NewType (sort);
        obj = NewObject (id, typ);
        typ->tag = obj->name;
        Enter (Display[DisplayLevel], obj);
     }
     else
     {
        /* type already exists */
        typ = obj->type;

        if (typ->sort!=sort || obj->storage==Typedef)
           ErrorId ("Type mismatch in redeclaration", id);

        if (def && typ->defined)
           ErrorId ("Multiple declaration", id);
     }
  }

  if (!def)
     typ = Alias (id, Tag, typ);

  return typ;
}

/******************************** ENUM ************************************/

TypePtr enum_specifier (void)
{
  IDENT_TYP id;
  TypePtr typ;
  ObjectPtr elem;

  CheckSymbol (ENUM_);

  if (sy==IDENT)
     ReadIdent (id);
  else
     id[0] = 0;

  typ = CreateType (id, Enum, sy==LBRACE);

  if (sy==LBRACE)
  {
     if (typ->defined) ErrorId ("Multiple declaration", id);
     typ->defined = true;
     NextSymbol ();

     /* enumerator_list */
     for (;;)
     {
        /* enumerator */
        ReadIdent (id);
        elem = NewObject (id, typ);
        Enter (Display[DisplayLevel], elem);
        Append (&typ->list, elem);

        if (sy==ASSIGN)
        {
           NextSymbol ();
           elem->init = const_expr ();
        }
        if (sy!=COMMA) break;
        NextSymbol ();
     }
     CheckSymbol (RBRACE);
  }

  return typ;
}

/******************************* STRUCTURE ********************************/

TypePtr struct_specifier (void)
{
  IDENT_TYP id;
  sort_typ sort;
  TypePtr typ;
  Queue queue;

  if (sy==STRUCT_) sort = Struct;
  else if (sy==UNION_) sort = Union;
  else Bug ();

  NextSymbol ();

  if (sy==IDENT)
     ReadIdent (id);
  else
     id[0] = 0;

  typ = CreateType (id, sort, sy==LBRACE);

  if (sy==LBRACE)
  {
     if (typ->defined) ErrorId ("Multiple declaration", id);
     typ->defined = true;
     NextSymbol ();

     /* struct_declaration_list */
     /* struct_declaration */
     /* specifier_qualifier_list */
     /* struct_declarator_list */
     /* struct_declarator */

     OpenScope (&typ->scope);

     while (sy!=RBRACE)
     {
        declaration (&queue, Width);
        Join (&typ->list, &queue);
     }
     NextSymbol ();

     CloseScope ();
  }

  return typ;
}

/************************* DECLARATION SPECIFIER **************************/

void CheckSort (TypePtr t)
{
  if (t->sort!=NoSort)
     Error ("Too many types in declaration");
}

void CheckLength (TypePtr t)
{
  if (t->length!=NoLength)
     Error ("Too many types in declaration");
}

void CheckSign (TypePtr t)
{
  if (t->sign!=NoSign)
     Error ("Too many types in declaration");
}

static void CheckNumeric (TypePtr t)
{
  if (! IsNumericSort (t->sort))
     Error ("Too many types in declaration");
}

void CheckInt (TypePtr t)
{
  if (! IsIntegerSort (t->sort))
     Error ("Too many types in declaration");
}

void CheckAll (TypePtr t)
{
  if (t->sort!=NoSort ||
      t->length!=NoLength ||
      t->sign!=NoSign)
     Error ("Too many types in declaration");
}

/* ---------------------------------------------------------------------- */

TypePtr sim_declaration_specifiers (void)
{
  TypePtr typ;

  typ = NewType (NoSort);

  for (;;)
     /* simple_type_name */
     if (sy==CHAR_)
     {
        CheckSort (typ);
        typ->sort = Char;
        NextSymbol ();
     }
     else if (sy==INT_)
     {
        CheckSort (typ);
        typ->sort = Int;
        NextSymbol ();
     }
     else if (sy==FLOAT_)
     {
        CheckSort (typ);
        CheckSign (typ);
        typ->sort = Float;
        NextSymbol ();
     }
     else if (sy==DOUBLE_)
     {
        CheckSort (typ);
        CheckSign (typ);
        typ->sort = Double;
        NextSymbol ();
     }
     else if (sy==VOID_)
     {
        CheckSort (typ);
        CheckSign (typ);
        CheckLength (typ);
        typ->sort = Void;
        NextSymbol ();
     }

     else if (sy==SHORT_)
     {
        CheckLength (typ);
        CheckNumeric (typ);
        typ->length = Short;
        NextSymbol ();
     }
     else if (sy==LONG_)
     {
        CheckLength (typ);
        CheckNumeric (typ);
        typ->length = Long;
        NextSymbol ();
     }

     else if (sy==SIGNED_)
     {
        CheckSign (typ);
        CheckInt (typ);
        typ->sign = Signed;
        NextSymbol ();
     }
     else if (sy==UNSIGNED_)
     {
        CheckSign (typ);
        CheckInt (typ);
        typ->sign = Unsigned;
        NextSymbol ();
     }
     else break;

  if (typ->sort==NoSort &&
      typ->sign==NoSign &&
      typ->length==NoLength)
     Error ("Type name expected");

  return typ;
}

bool IsType (ObjectPtr obj)
{
  sort_typ sort;

  if (obj==NULL) return false;
  if (obj->storage==Typedef) return true;
  sort = obj->type->sort;
  if (IsClsSort (sort) || sort==Enum) return true;
  return false;
}

TypePtr declaration_specifiers (void)
{
  TypePtr typ;
  ObjectPtr obj;

  if (sy==STRUCT_ || sy==UNION_)
     typ = struct_specifier ();
  else if (sy==ENUM_)
     typ = enum_specifier ();
  else
  {
     typ = NULL;
     if (sy==IDENT)
     {
        obj = Lookup (IdentVal);
	if (IsType (obj))
        {
	   NextSymbol ();
           typ = Alias (obj->name, Name, obj->type);
        }
     }

     if (typ==NULL)
        typ = sim_declaration_specifiers ();
  }

  return typ;
}

/******************************* MODIFIERS ********************************/

void type_qualifier_list (TypePtr typ)
{
  for (;;)
    /* type_qualifier */
    if (sy==CONST_)
       typ->Const = true;
    else if (sy==VOLATILE_)
       typ->Volatile = true;

    else break;
}

/******************************* DECLARATOR *******************************/

ObjectPtr declarator (TypePtr typ)
{
  ObjectPtr obj;
  IDENT_TYP id;
  TypePtr ref;

  /* pointer or reference */
  while (sy==ASTERISK || sy==AND)
  {
     /* declaration_specifiers */
     if (sy==ASTERISK)
     {
        NextSymbol ();
        typ = Link (Pointer, typ);
     }
     else if (sy==AND)
     {
        NextSymbol ();
        typ = Link (Reference, typ);
     }
     else Bug ();

     type_qualifier_list (typ); /* !!! */
  }

  /* direct_declarator */
  if (sy==LPAR)
  {
      NextSymbol ();
      obj = declarator (NULL);
      CheckSymbol (RPAR);
  }
  else if (sy==IDENT)
  {
     ReadIdent (id);
     obj = NewObject (id, NULL);
  }
  else
  {
     /* abstract declarator */
     obj = NewObject ("", NULL);
  }

  for (;;)
     if (sy==LBRACK)
     {
        NextSymbol ();
        typ = Link (Array, typ);
        typ->range = const_expr ();
        CheckSymbol (RBRACK);
     }

     else if (sy==LPAR)
     {
        int cnt = 0;

        NextSymbol ();
        typ = Link (Function, typ);

        /* parameter_type_list, parameter_list */
        for (;;)
           if (sy==RPAR)
              break;
           else if (sy==DOTS)
           {
              ObjectPtr pobj;

              pobj = NewObject ("...", NULL);
              Append (&typ->list, pobj);

              NextSymbol ();
              break;
           }
           else
           {
              TypePtr ptyp;
              ObjectPtr pobj;

              ptyp = declaration_specifiers ();
              pobj = declarator (ptyp);

              /* add parameter */
              Enter (&typ->scope, pobj);
              Append (&typ->list, pobj);

              cnt ++;
              if (ptyp->sort==Void)
              {
                 if (cnt!=1)
                    Error ("Invalid parameter type (void)");
                 break;
              }

              if (sy!=COMMA) break;
              NextSymbol ();
           }

        CheckSymbol (RPAR);
     }
     else break;

  /* link 'obj' and 'typ' */
  Check (obj!=NULL);

  if (obj->type==NULL)
  {
     obj->desc = typ;
     obj->type = Resolve (typ);
  }
  else
  {
     ref = obj->type;
     while (ref->from!=NULL) ref=ref->from;
     ref->desc = typ;
     ref->from = Resolve (typ);
  }

  return obj;
}

/******************************** IS DECL *********************************/

bool DclSym [SYMBOL_MAX+1];

void InitDclSym (void)
{
  DclSym [CHAR_] = 1;
  DclSym [INT_] = 1;
  DclSym [FLOAT_] = 1;
  DclSym [DOUBLE_] = 1;
  DclSym [VOID_] = 1;

  DclSym [SHORT_] = 1;
  DclSym [LONG_] = 1;

  DclSym [SIGNED_] = 1;
  DclSym [UNSIGNED_] = 1;

  DclSym [CONST_] = 1;
  DclSym [VOLATILE_] = 1;

  DclSym [ENUM_] = 1;
  DclSym [STRUCT_] = 1;
  DclSym [UNION_] = 1;

  DclSym [TYPEDEF_] = 1;
  DclSym [AUTO_] = 1;
  DclSym [REGISTER_] = 1;
  DclSym [STATIC_] = 1;
  DclSym [EXTERN_] = 1;
}

bool IsDecl (void)
{
  ObjectPtr obj;
  TypePtr   typ;
  sort_typ  sort;

  if (sy==IDENT)
  {
     obj = Lookup (IdentVal);
     if (obj==NULL) return (false);

     typ = obj->type;
     if (typ==NULL) return (false);

     sort=typ->sort;

     return (obj->storage==Typedef || IsClsSort (sort));
  }
  else
  {
     return (DclSym [sy]);
  }
}

/******************************* FUNCTIONS ********************************/

void FunctionBody (ObjectPtr object)
{
  OpenScope (&object->type->scope);
  Fce = object;

  object->body = statement ();
  if (sy==SEMICOLON) NextSymbol ();

  Fce = NULL;
  CloseScope ();
}

/****************************** DECLARATION *******************************/

void SetStorage (storage_typ * var, storage_typ value)
{
  if (*var!=NoStorage)
     Error ("Too many storage classes in declaration");
  *var = value;
  NextSymbol ();
}

storage_typ storage_class_specifier (void)
{
  storage_typ storage = NoStorage;

  if (DisplayLevel==0)
     if (sy==AUTO_ || sy==REGISTER_)
        Error ("Invalid storage class");

  if (sy==TYPEDEF_)
     SetStorage (&storage, Typedef);
  else if (sy==EXTERN_)
     SetStorage (&storage, Extern);
  else if (sy==AUTO_)
     SetStorage (&storage, Auto);
  else if (sy==REGISTER_)
     SetStorage (&storage, Register);
  else if (sy==STATIC_)
     SetStorage (&storage, Static);

  return (storage);
}

#define OPT(n) ((opt&(n))!=0)

void declaration (QueuePtr queue, int opt)
{
  storage_typ storage;
  ObjectPtr   obj;
  TypePtr     type;
  sort_typ    sort;

  Clear (queue);

  storage = storage_class_specifier ();
  type = declaration_specifiers ();

  /* init_declarator_list */
  for (;;)
  {
     /* init_declarator */
     obj = declarator (type);

     Enter (Display[DisplayLevel], obj);

     Append (queue, obj);
     obj->storage = storage;
     if (storage==Typedef)
        obj->type->tag = obj->name;

     sort = obj->type->sort;
     if (sort==Void)
        Error ("Invalid type");

     obj->fce = storage!=Typedef &&
                (sort==Function || sort==Constructor || sort==Destructor);

     if (obj->fce) break;

     if (OPT (Width) && sy==COLON)
     {
        NextSymbol ();
        obj->width = const_expr ();
     }

     if (OPT (Init) && sy==ASSIGN)
     {
        NextSymbol ();
        obj->init = initializer ();
     }

     if (sy!=COMMA) break;
     NextSymbol ();
  }

  if (obj->fce && sy==LBRACE)
  {
     if (OPT (Body))
        FunctionBody (obj);
     else
        Error ("Function definition is not allowed here");
  }
  else
  {
     CheckSymbol (SEMICOLON);
  }
}

/************************* GLOBAL DECLARATION **************************/

void GlobalDecl (void)
{
  Queue queue;

  /* read declaration */
  declaration (&queue, Body | Init);
  Join (&glist, &queue);
}

/******************************** PUT DECL ********************************/

enum mode_typ { header, full, param };
void SubDecl (ObjectPtr obj, mode_typ mode);

void Separator (TypePtr t, bool * ptr, bool * mark)
{
  *mark = false;

  switch (t->sort)
  {
     case Pointer:
     case Reference:
        *ptr = true;
        break;

     case Array:
     case Function:
        if (*ptr) *mark = true;
        *ptr = false;
        break;
  }
}

bool spc = false;

void PutSpc (char *s)
{
  if (spc) PutChr (' ');
  PutStr (s);
  spc = true;
}

void Prefix (TypePtr t, TypePtr desc, bool ptr)
{
  bool mark;
  NodePtr p;

  Check (t!=NULL);
  if (desc!=NULL) t=desc;

  if (t->from!=NULL && t->sort!=Name && t->sort!=Tag)
  {
     Separator (t, &ptr, &mark);
     Prefix (t->from, t->desc, ptr);
     if (mark) PutSpc ("(");
  }

  if (t->Const) PutSpc ("const");
  if (t->Volatile) PutSpc ("volatile");

  switch (t->sign)
  {
     case Signed:   PutSpc ("signed"); break;
     case Unsigned: PutSpc ("unsigned"); break;
  }

  switch (t->length)
  {
     case Short: PutSpc ("short"); break;
     case Long:  PutSpc ("long");  break;
  }

  switch (t->sort)
  {
     case Char:   PutSpc ("char");   break;
     case Int:    PutSpc ("int");    break;
     case Float:  PutSpc ("float");  break;
     case Double: PutSpc ("double"); break;
     case Void:   PutSpc ("void");   break;

     case Pointer:
        PutSpc ("*");
        break;

     case Reference:
        PutSpc ("&");
        break;

     case Struct:
     case Union:
     case Class:

        if (t->sort==Struct) PutSpc ("struct");
        else if (t->sort==Union) PutSpc ("union");
        else PutSpc ("class");

        if (t->tag!=NULL)
        {
           PutChr (' ');
           PutStr (t->tag);
        }

        PutEol ();

        PutChr ('{');
        PutEol ();
        Indent ();

        for (p=t->list.first; p!=NULL; p=p->next)
        {
           Check (p->item!=NULL);
           SubDecl (p->item, header);
        }

        Unindent ();
        PutChr ('}');
        PutChr (' ');
        break;

     case Enum:
        PutSpc ("enum");
        if (t->tag)
        {
           PutChr (' ');
           PutStr (t->tag);
        }

        PutEol ();
        PutChr ('{');
        PutEol ();
        Indent ();

        for (p=t->list.first; p!=NULL; p=p->next)
        {
           PutStr (p->item->name);
           if (p->next!=NULL) PutStr (", ");
           PutEol ();
        }

        Unindent ();
        PutChr ('}');
        PutChr (' ');
        break;

     case Name:
        PutSpc (t->tag);
        break;

     case Tag:
        switch (t->from->sort)
        {
           case Struct: PutSpc ("struct"); break;
           case Union:  PutSpc ("union"); break;
           case Enum:   PutSpc ("enum"); break;
           default:     Bug ();
        }
        PutSpc (t->tag);
        break;
  }
}

void Postfix (TypePtr t, TypePtr desc, bool ptr)
{
  bool mark;
  NodePtr p;

  Check (t!=NULL);
  if (desc!=NULL)t=desc;

  if (t->sort==Name || t->sort==Tag) return;

  if (t->from!=NULL)
  {
     Separator (t, &ptr, &mark);
     if (mark) PutChr (')');
  }

  switch (t->sort)
  {
     case Array:
        PutStr (" [");
        if (t->range!=NULL) PutExpr (t->range);
        PutStr ("]");
        break;

     case Function:
     case Constructor:
     case Destructor:
        PutStr (" (");
        for (p=t->list.first; p!=NULL; p=p->next)
        {
           SubDecl (p->item, param);
           if (p->next!=NULL) PutStr (", ");
        }
        PutStr (")");
        break;
  }

  if (t->from!=NULL)
  {
     Postfix (t->from, t->desc, ptr);
  }
}

void PutStorage (storage_typ storage, mode_typ mode)
{
  switch (storage)
  {
     case Typedef:  PutStr ("typedef "); break;
     case Auto:     PutStr ("auto "); break;
     case Register: PutStr ("register "); break;
     case Static:   PutStr ("static "); break;
     case Extern:   if (mode!=full) PutStr ("extern "); break;
  }
}

void SubDecl (ObjectPtr obj, mode_typ mode)
{
  PutStorage (obj->storage, mode);

  spc = false;
  Prefix (obj->type, obj->desc, false);
  PutSpc (obj->name);
  Postfix (obj->type, obj->desc, false);

  if (obj->width!=NULL)
  {
     PutChr (':');
     PutExpr (obj->width);
  }

  if (obj->init!=NULL)
  {
     PutChr ('=');
     PutExpr (obj->init);
  }

  if (mode==header)
  {
     PutChr (';');
     PutEol ();
  }

  if (mode==full)
  {
     if (obj->fce)
     {
        PutEol ();
        if (obj->body!=NULL)
           PutStat (obj->body);
     }
     else
     {
        PutChr (';');
        PutEol ();
     }
  }
}

void PutDecl (ObjectPtr obj)
{
  SubDecl (obj, full);
}

void PutHeader (ObjectPtr obj)
{
  SubDecl (obj, header);
}

void PutGlobalDecl (void)
{
  NodePtr p;
  for (p=glist.first; p!=NULL; p=p->next)
     PutDecl (p->item);
}

void PutGlobalHeader (void)
{
  NodePtr p;
  for (p=glist.first; p!=NULL; p=p->next)
     PutHeader (p->item);
}
