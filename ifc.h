#ifndef IFC_H
#define IFC_H

#include <string>

std::string getInpText ();

void clearOutLines ();
void showOutLine (std::string text);

void showError (std::string msg);

#endif // IFC_H
