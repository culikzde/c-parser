
/* OUT.C */

#include "inp.h"
#include "out.h"

#include "ifc.h"

#include <string>
using std::string;

/********************************* OUTPUT *********************************/

static int    indent;  /* pocet mezer na zacatku radky */
static int    OutPos;  /* pocet znaku ve vystupni radce */
static string OutLine; /* vystupni radka */

void OpenOut (void)
/* otevri vystupni soubor */
{
  clearOutLines (); /* vymaz vystupni okno */
  indent = 0;
  OutPos = 0;
  OutLine = "";
}

void CloseOut (void)
/* uzavri vystupni soubor */
{
  PutEol ();
}

/****************************** INDENTATION *******************************/

void Indent (void)
/* zvetsi pocet mezer na zacatku radky */
{
  if (indent==0)
     indent = 2;
  else
     indent += 3;
}

void Unindent (void)
/* zmensi pocet mezer na zacatku radky */
{
  if (indent<3)
     indent = 0;
  else
     indent -= 3;
}

/********************************** PUT ***********************************/

void IndentLine (void)
{
  if (OutLine.length() == 0)
  {
     for (int i = 1; i <= indent; i++)
        OutLine = OutLine + " ";
  }
}

void PutEol (void)
{
  showOutLine (OutLine);
  OutLine = "";
}

void PutChr (char c)
{
  if (c != CR && c != LF)
  {
     IndentLine ();
     OutLine = OutLine + c;
  }
  if (c == LF) PutEol ();
}

void PutStr (char * txt)
{
  IndentLine ();
  OutLine = OutLine + txt;
}
