
/* DECL.H */

#ifndef DECL_H
#define DECL_H

#include "inp.h"
#include "out.h"
#include "lex.h"

struct Node;
struct Queue;
struct Scope;
struct Object;
struct Type;
struct Expr;
struct Stat;

typedef Node *   NodePtr;
typedef Queue *  QueuePtr;
typedef Scope *  ScopePtr;
typedef Object * ObjectPtr;
typedef Type *   TypePtr;
typedef Expr *   ExprPtr;
typedef Stat *   StatPtr;

/******************************* UTILITIES ********************************/

#define NEW(type) (type *) New (sizeof (type))
void * New (unsigned size); /* allocate and clear memory */
char * NewStr (char * s);   /* allocate memory for string */

typedef char * IDENT_PTR;
IDENT_PTR ReadIdentPtr (void);
void ReadIdent (IDENT_TYP id);

void ErrorId (char * msg, char * id);
void Bug (void);
void Check (bool b);

/********************************** INIT **********************************/

void InitDecl (void);

/********************************* QUEUE **********************************/

struct Node                    /* Element of queue of objects */
{
   NodePtr    next;
   NodePtr    prev;
   ObjectPtr  item;
};

struct Queue                   /* Queue of objects */
{
   NodePtr first;
   NodePtr last;
};

void Clear (Queue * queue);
/* Initialize queue */

void Add (Queue * queue, NodePtr node);
/* Add node to queue */

void Append (Queue * queue, ObjectPtr obj);
/* Add object to queue */

void Join (Queue * dst, Queue * add);
/* Join two queues */

/********************************* SCOPE **********************************/

struct Scope
{
   ObjectPtr root;                          /* tree root */
};

extern Scope gscope;                        /* tree of global identifiers */
extern Queue glist;                         /* queue of global identifiers */
extern ObjectPtr Fce;                       /* current function */

#define DISPLAY_MAX 10
extern int DisplayLevel;                    /* number of current scopes */
extern ScopePtr DisplayArray [DISPLAY_MAX]; /* array of scopes */

void OpenScope (ScopePtr s);                /* add new scope */
void CloseScope (void);                     /* remove last scope */

/********************************** TREE ***********************************/

void Enter (ScopePtr scope, ObjectPtr add);
/* Enter object into tree */

ObjectPtr Search (ObjectPtr root, IDENT_TYP name);
/* Search for identifier in tree, return NULL if not found */

ObjectPtr Lookup (IDENT_TYP name);
/* Search for identifier in all scopes, return NULL if not found */

ObjectPtr Find (IDENT_TYP name);
/* Search for identifier in all scopes, error if not found */

ObjectPtr LookupMember (TypePtr typ, IDENT_TYP name);
/* Search for identifier in structure, return NULL if not found */

ObjectPtr FindMember (TypePtr typ, IDENT_TYP name);
/* Search for identifier in structure, error if not found */

/********************************* OBJECT *********************************/

enum storage_typ { NoStorage, Typedef,
                   Auto, Register, Static, Extern };

struct Object                  /* Object = typedef, variable, function, ... */
{
   ObjectPtr   left;           /* pointers for tree */
   ObjectPtr   right;

   char *      name;           /* identifier */
   TypePtr     type;           /* type */
   TypePtr     desc;           /* detail type description */

   storage_typ storage;        /* storage class */
   ExprPtr     init;           /* initializer */
   ExprPtr     width;          /* width of bit field */

   StatPtr     body;           /* function definition */
   bool        fce;            /* function */
};

/********************************** TYPE **********************************/

enum sort_typ  { NoSort,
                 Char, Enum, Int, Float, Double, Pointer, Reference,
                 Array, Function, Struct, Union, Class, Void,
                 Constructor, Destructor,
                 Name, Tag };


inline bool IsIntegerSort (sort_typ sort)
{ return sort<=Int; }

inline bool IsFloatSort (sort_typ sort)
{ return sort==Float || sort==Double; }

inline bool IsNumericSort (sort_typ sort)
{ return sort<=Double; }

inline bool IsOperandSort (sort_typ sort)
{ return sort<=Pointer; }

inline bool IsPtrSort (sort_typ sort)
{ return sort==Pointer || sort==Reference || sort==Array; }

inline bool IsClsSort (sort_typ sort)
{ return sort==Struct || sort==Union || sort==Class; }

enum length_typ  { NoLength, Short, Long };
enum sign_typ    { NoSign, Signed, Unsigned };

struct Type
{
   sort_typ    sort;    /* NoSort, Char, Enum, Int, ... */
   sign_typ    sign;    /* NoSign, Signed, Unsigned */
   length_typ  length;  /* NoLength, Short, Long */
   bool        Const;
   bool        Volatile;

   TypePtr     from;    /* for arrays - type of element */
                        /* pointers - base type */
                        /* functions - type of result */

   TypePtr     desc;    /* detail type description */

   ExprPtr     range;   /* for arrays - number of elements */

   Scope       scope;   /* for structures - tree of fields */
                        /* for fuctions - tree of local identifiers */

   Queue       list;    /* for enums - list of elements */
                        /* for structures - fields */
                        /* for functions - parameters */

                        /* for structures and enums */
   char *      tag;     /* tag name */
   bool        defined; /* true => already defined */
};

TypePtr   NewType (sort_typ sort);
TypePtr   Resolve (TypePtr typ);
TypePtr   Link (sort_typ sort, TypePtr desc);
TypePtr   Alias (IDENT_TYP name, sort_typ sort, TypePtr desc);
ObjectPtr NewObject (IDENT_TYP name, TypePtr desc);
TypePtr   CreateType (IDENT_TYP id, sort_typ sort, bool def);

/****************************** DECLARATIONS ******************************/

const int Body    = 1;   /* function body { ... } */
const int Init    = 2;   /* variable initializer */
const int Width   = 4;   /* field width */

bool IsDecl (void);                        /* is it declaration */
void declaration (Queue * queue, int opt); /* read declaration */
void GlobalDecl (void);                    /* read global declaration */

void PutDecl (ObjectPtr obj);              /* print declaration */

void PutGlobalDecl (void);                 /* print global decl. */
void PutGlobalHeader (void);               /* print headers */

/******************** EXPRESSION - AUXILIARY ROUTINES *********************/

extern Type IntDef,                        /* standard types */
            UnsignedDef,
            LongDef,
            UnsignedLongDef,
            FloatDef,
            DoubleDef,
            LongDoubleDef,
            VoidDef,
            BoolDef,
            CharDef,
            StringDef;

bool IsInteger (TypePtr t);
bool IsFloat (TypePtr t);
bool IsNumeric (TypePtr t);
bool IsPointer (TypePtr t);
bool IsPtr (TypePtr t);
bool IsOperand (TypePtr t);
bool IsString (TypePtr t);

void CheckInteger (TypePtr t);
void CheckOperand (TypePtr t);
void CheckOperandExt (TypePtr t);
void CheckString (TypePtr t);
void CheckLValue (ExprPtr e);
void CheckAssign (TypePtr t1, TypePtr t2);
void CheckCall (TypePtr t, ExprPtr exp);

/******************************* EXPRESSION *******************************/

struct Expr
{
  SYMBOL   op;                 /* operator */
  char *   id;                 /* identifier */
  ExprPtr  e1, e2, e3;         /* sub-expressions */
  TypePtr  type;               /* type of result */
};

/* expression encoding:

   IDENT         id
   NUM           id
   FLT           id
   STR           id
   CHR           id

   LBRACK        e1 [e2]
   LPAR          (e1)
   FCE           e1 (e2)
   PERIOD        e1.id
   ARROW         e1->id

   DPLUS         e1 ++     (e2==NULL)
   DMINUS        e1 --

   DPLUS         ++ e2     (e1==NULL)
   DMINUS        -- e2

   PLUS          + e2
   MINUS         - e2
   NOT           ~ e2
   LOGNOT        ! e2
   ADR           & e2
   DEREF         * e2

   SIZEOF_       sizeof (e1)

   ASTERISK      e1 * e2
   SLASH         e1 / e2
   MOD           e1 % e2

   PLUS          e1 + e2
   MINUS         e1 - e2

   SHL           e1 << e2
   SHR           e1 >> e2

   LESS          e1 < e2
   GREATER       e1 > e2
   LESSEQUAL     e1 <= e2
   GREATEREQUAL  e1 >= e2

   EQUAL         e1 == e2
   UNEQUAL       e1 != e2

   AND           e1 & e2
   XOR           e1 ^ e2
   OR            e1 | e2
   LOGAND        e1 && e2
   LOGOR         e1 || e2
   QUESTION      e1 ? e2 : e3

   ASSIGN        e1 = e2
   MULASSIGN     e1 *= e2
   DIVASSIGN     e1 /= e2
   MODASSIGN     e1 %= e2
   ADDASSIGN     e1 += e2
   SUBASSIGN     e1 -= e2
   SHLASSIGN     e1 <<= e2
   SHRASSIGN     e1 >>= e2
   ANDASSIGN     e1 &= e2
   XORASSIGN     e1 ^= e2
   ORASSIGN      e1 |= e2

   COMMA         e1, e2

   LBRACE        {e1}
*/

ExprPtr expression (void);   /* read any expression */
ExprPtr const_expr (void);   /* read constant expression */
ExprPtr simple_expr (void);  /* read expression without , */
ExprPtr initializer (void);  /* read initializer */

void PutExpr (ExprPtr t);    /* print expression */

/******************************* STATEMENT ********************************/

struct Stat
{
  SYMBOL    st;          /* type of statement */

  char *    id;          /* label identifier */
  ExprPtr   e1, e2, e3;  /* expressions in statement */
  StatPtr   s1, s2;      /* inner statements */
  StatPtr   next;        /* next statement in { } */

  Scope     scope;       /* tree of local identifiers in { } */
  Queue     decl;        /* queue of declarations (when st==DECL) */
};

/* statement encoding:

   GOTO_      goto id;
   CONTINUE_  continue;
   BREAK_     break;
   RETURN_    return e1;
   WHILE_     while (e1) s1
   DO_        do s1 while (e1)
   FOR_       for (e1;e2;e3) s1
   IF_        if (e1) s1 else s2
   SWITCH_    switch (e1) s1
   LBRACE     { s1 s1->next ... }
   CASE_      case e1:
   DEFAULT_   default:

   LAB        e1:s1
   SIM        e1;
   DECL       decl.first ...
*/

StatPtr statement (void);       /* read statement */
void    PutStat (StatPtr s);    /* print statement */

#endif
